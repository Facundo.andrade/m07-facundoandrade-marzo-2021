<!DOCTYPE html>

<?php require_once ('../helpers/i18n.php'); ?>
<html lang="en">
<link rel="stylesheet" href="../Css/global.css"

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo _("Registro"); ?></title>
</head>

<body>
<?php require_once('header.php'); ?>
<form action="../controller/controller.php" method="post">
    <p style="text-align: center"><?php echo _("Idioma Castellano"); ?></p>
    <input name="userName" type="text" placeholder="Name">
    <br/>
    <input name="userSurname" type="text" placeholder="Surname">
    <br/>
    <select name="genre">
        <option value="h">H</option>
        <option value="m">M</option>
    </select>
    <br/>
    <input name="birthdate" type="date">
    <br/>
    <input name="dni" type="text" placeholder="DNI">
    <br/>
    <input name="mobileNumber" type="tel" placeholder="Mobile number">
    <br/>
    <input name="email" type="email" placeholder="email">
    <br/>
    <input name="pass" type="password" placeholder="Password">
    <br/>
    <input name="control" value="register" type="hidden">
    <br/>
    <input name="submit" value="submit" type="submit">
</form>

<?php
if (isset($_POST['message']))
    echo $_POST['message'] . '<br/>';
if (isset($datos['algo']))
    echo $datos['algo'] . '<br/>';

if ( isset($_GET['a']) )
    echo $_GET['a'] ;

?>
</body>

</html>

