<?php

require_once('../helpers/DBManager.php');


function getSaldo($cuenta){
    $manager = new DBManager();
    try {
        $sql = "SELECT saldo FROM cuenta WHERE iban=:iban";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':iban',$cuenta);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $manager->cerrarConexion();

        return $rt[0]['saldo'];

    }catch(PDOException $e){
        echo $e->getMessage();
    }

}

function getLastId(){
    $manager = new DBManager();
    try {
        $sql = "SELECT id FROM cuenta ORDER BY id DESC limit 1";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (sizeof($rt)>0){
            return $rt[0]['id'];
        }else{
            return 0;
        }
        $manager->cerrarConexion();

    }catch(PDOException $e){
        echo $e->getMessage();
    }

}

function getUserId($dni)
{
    $manager = new DBManager();
    try {
        $sql = "SELECT id FROM cliente WHERE dni=:dni";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if (sizeof($rt) > 0) {
            return $rt[0]['id'];
        } else {
            return 0;
        }
       //  $manager->cerrarConexion();
    }catch(PDOException $e){
        echo $e->getMessage();
    }
}


function createAccount($dni){

    $id = getUserId($dni);
    $manager = new DBManager();
    $rt = null;
    try {
        //recuperar el último id de cuenta y con el id generamos el iban que tiene 24 dígitos
        $lastId=getLastId()+1;
        $len=strlen($lastId);
        $iban='';
        for ($i=1;$i<24-$len;$i++){
            $iban.='0';
        }
        $iban.=$lastId;

        //Insertamos en base de datos
        $sql = "INSERT INTO cuenta (id_cliente,saldo) VALUES (:id,1000)";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        //$manager->cerrarConexion();

    }catch(PDOException $e){
        echo $e->getMessage();
    }

}

function getAccounts($dni){
    $manager = new DBManager();
    try {
        $sql = "SELECT * FROM cuenta WHERE id_cliente=:id_cliente";
        $stmt = $manager->getConexion()->prepare($sql);
        $id_cliente=getUserId($dni);
        error_log("---------------" . $dni);

        $stmt->bindParam(':id_cliente',$id_cliente);
        $stmt->execute();
        $rt = $stmt->fetchAll(PDO::FETCH_ASSOC);
        error_log("---------------" . $rt);
        return $rt;

//        $manager->cerrarConexion();

    }catch(PDOException $e){
        echo $e->getMessage();
    }

}

?>