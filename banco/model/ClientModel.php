<?php


require_once('../helpers/DBManager.php');

use DBManager;

function insertCliente($cliente)
{

    $manager = new DBManager();
    try {
        $sql = "INSERT INTO cliente (nombre, nacimiento, apellidos, sexo, email, telefono, dni, password) VALUES (:nombre, :nacimiento, :apellidos, :sexo, :email, :telefono,:dni,:password)";

        $password = password_hash($cliente->getPassword(), PASSWORD_DEFAULT, ['cost' => 10]);

        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindValue(':nombre', $cliente->getNombre());
        $stmt->bindValue(':apellidos', $cliente->getApellidos());
        $stmt->bindValue(':nacimiento', date('d/m/Y',strtotime($cliente->getFechaNacimiento())));
        $stmt->bindValue(':sexo', $cliente->getSexo());
        $stmt->bindValue(':telefono', $cliente->getTelefono());
        $stmt->bindValue(':dni', str_replace('-','',$cliente->getDni()));
        $stmt->bindValue(':email', $cliente->getEmail());
        $stmt->bindValue(':password', $password);

        if ($stmt->execute()) {
            error_log("todo OK");
        } else {
            error_log("MAL" .  $cliente->getSexo());
        }

    } catch (PDOException $e) {
        echo $e->getMessage();
    }

}

function getUserHash($dni){
    $conexion = new DBManager();
    try{
        $sql = "SELECT * FROM cliente WHERE dni=:dni";
        $stmt = $conexion->getConexion()->prepare($sql);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
        $result  = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result[0]['password'];
    }catch (PDOException $e){
        echo $e->getMessage();
    }


}

function selectCliente($dni){
    $manager = new DBManager();
    try{
        $id = getUserId($dni);
        $sql="SELECT * FROM cliente WHERE id=:id";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':id',$id);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $obj = new Cliente($result[0]['nombre'],$result[0]['apellidos'],$result[0]['nacimiento'],$result[0]['sexo'],$result[0]['email'],$result[0]['telefono'],$result[0]['dni'],$result[0]['password'],$result[0]['image']);
        return $obj;
    }catch ( PDOException $e){
        echo $e->getMessage();
    }

}

function updateCliente($image,$dni){
    $manager = new DBManager();
    try{
        $id = getUserId($dni);
        $sql="UPDATE cliente SET image=:img WHERE id=:id";
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindParam(':img',$image,PDO::PARAM_LOB);
        $stmt->bindParam(':id',$id);

        $stmt->execute();
    }catch ( PDOException $e){
        echo $e->getMessage();
    }

}

?>
