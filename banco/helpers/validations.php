<?php

function validate()
{
    $message = validateName($_POST['userName']) . validateSurname($_POST['userSurname']) . validateDate($_POST['birthdate']) . validateDni($_POST['dni']) . validateTel($_POST['mobileNumber']) . validatePass($_POST['pass'], $_POST['AuxPass']);
    $_POST['Error']= $message;
    if ($message != null){
        echo $message;
        return false;
    }else{
        return true;
    }
}

//------------Validaciones------------------
function validateName($userName)
{
    if ($userName == '') {
        return "Introduce tu nombre </br>";
    } else  if (preg_match('/[^a-z\s]/i', $userName)) {
        return "Campo nombre incorrecto. </br>";
    }


}

function validateSurname($userSurname)
{
    if ($userSurname == '') {
        return "Introduce el apellido </br>";
    } else if (preg_match('/[^a-z\s]/i', $userSurname)) {
        return 'Campo apellido incorrecto. </br>';
    }
}

function validateDate($birthdate)
{
    $edad = date_diff(date_create($birthdate), date_create('today'))->y;
    if ($edad < 18) {
        return "No puedes ser menor de 18 años. </br>";
    }
}

function validateDni($dni)
{
    $parteDni = explode("-", $dni);
    if ($dni == '') {
        return "Introduce tu DNI. </br>";
    } else if (count($parteDni) > 2 || count($parteDni) == 1) {
        echo count($parteDni);
        return "Campo DNI incorrecto. </br>";
    } else if (numDni($parteDni) == false) {
        return "Letra DNI incorrecta. </br>";
    }
}

function validateTel($mobileNumber)
{
    $numTel = str_split($mobileNumber);
    if ($mobileNumber == '') {
        return "Introduce el telefono. </br>";
    } else if (count($numTel) != 9 || $numTel[0] != 6 && $numTel[0] != 7) {
        return "Campo telefono incorrecto. </br>";
    }
}

function validatePass($pass, $AuxPass)
{
    $algo = ['0', '0', '0', '0'];
    $longPass = str_split($pass);

    if ($longPass < 8) {
        return "La contraseña tiene que tener mas de 7 caracteres. </br>";
    } else {
        for ($i = 0; count($longPass) > $i; $i++) {
            if (preg_match('/[A-Z]/', $longPass[$i])) {
                $algo[0] = '1';
            }
            if (preg_match('/[a-z]/', $longPass[$i])) {
                $algo[1] = '1';
            }
            if (preg_match('/[0-9]/', $longPass[$i])) {
                $algo[2] = '1';
            }
            if (preg_match('/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', $longPass[$i])) {
                $algo[3] = '1';
            }
        }
        if ($algo[0] != 1 || $algo[1] != 1 || $algo[2] != 1 || $algo[3] != 1) {
            return "La contraseña no cumple los requisitos: Que contenga mayúsculas, minúsculas, números, un carácter especial. </br>";
        }
    }
}

function numDni($parteDni)
{
    $numDni = str_split($parteDni[0]);
    if (count($numDni) == 8) {
        if (letraDni($parteDni) == true) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function letraDni($parteDni)
{
    $letra = ['T', 'R', 'W', 'A', 'G', 'H', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'];
    $result = $parteDni[0] % 23;
    if ($letra[$result] == strtoupper($parteDni[1])) {
        return true;
    } else {
        return false;
    }
}
?>